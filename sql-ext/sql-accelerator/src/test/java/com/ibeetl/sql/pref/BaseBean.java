package com.ibeetl.sql.pref;

import lombok.Data;

@Data
public class BaseBean<T> {
	private T id;
}
