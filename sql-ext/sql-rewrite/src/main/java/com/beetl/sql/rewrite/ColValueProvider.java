package com.beetl.sql.rewrite;

public interface ColValueProvider {
	public Object getCurrentValue();
}
