package org.beetl.sql.ymtraix;

import lombok.Data;

@Data
public class Color {
	String ab;
	String desc;
}
