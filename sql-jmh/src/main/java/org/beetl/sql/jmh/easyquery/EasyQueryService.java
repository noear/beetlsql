package org.beetl.sql.jmh.easyquery;

import com.easy.query.api4j.client.DefaultEasyQuery;
import com.easy.query.api4j.client.EasyQuery;
import com.easy.query.core.api.client.EasyQueryClient;
import com.easy.query.core.api.pagination.EasyPageResult;
import com.easy.query.core.bootstrapper.EasyQueryBootstrapper;
import com.easy.query.core.configuration.nameconversion.NameConversion;
import com.easy.query.core.configuration.nameconversion.impl.UnderlinedNameConversion;
import com.easy.query.h2.config.H2DatabaseConfiguration;
import org.beetl.sql.jmh.base.BaseService;
import org.beetl.sql.jmh.base.DataSourceHelper;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * create time 2023/8/3 08:34
 * 文件说明
 *
 * @author xuejiaming
 */
public class EasyQueryService implements BaseService {
	EasyQueryClient easyQueryClient = null;
	EasyQuery easyQuery = null;
	AtomicInteger idGen = new AtomicInteger(1000);
	public void init(){
		DataSource dataSource = DataSourceHelper.newDatasource();
		easyQueryClient = EasyQueryBootstrapper.defaultBuilderConfiguration()
			.setDefaultDataSource(dataSource)
			.optionConfigure(op -> {
				op.setPrintSql(false);
			})
			.useDatabaseConfigure(new H2DatabaseConfiguration())
			.replaceService(NameConversion.class, UnderlinedNameConversion.class)
			.build();
		easyQuery = new DefaultEasyQuery(easyQueryClient);
	}
	@Override
	public void addEntity() {
		EasyQuerySysUser easyQuerySysUser = new EasyQuerySysUser();
		easyQuerySysUser.setId(idGen.getAndIncrement());
		easyQuerySysUser.setCode("abc");
		easyQuerySysUser.setCode1("abc");
		easyQuerySysUser.setCode2("abc");
		easyQuerySysUser.setCode3("abc");
		easyQuerySysUser.setCode4("abc");
		easyQuerySysUser.setCode5("abc");
		easyQuerySysUser.setCode6("abc");
		easyQuerySysUser.setCode7("abc");
		easyQuerySysUser.setCode8("abc");
		easyQuerySysUser.setCode9("abc");
		easyQuerySysUser.setCode10("abc");
		easyQuerySysUser.setCode11("abc");
		easyQuerySysUser.setCode12("abc");
		easyQuerySysUser.setCode13("abc");
		easyQuerySysUser.setCode14("abc");
		easyQuerySysUser.setCode15("abc");
		easyQuerySysUser.setCode16("abc");
		easyQuerySysUser.setCode17("abc");
		easyQuerySysUser.setCode18("abc");
		easyQuerySysUser.setCode19("abc");
		easyQuerySysUser.setCode20("abc");
		easyQuery.insertable(easyQuerySysUser).executeRows();
	}

	@Override
	public Object getEntity() {
		return easyQueryClient.queryable(EasyQuerySysUser.class).whereById(1).firstOrNull();
	}

	@Override
	public void lambdaQuery() {

		List<EasyQuerySysUser> list = easyQuery.queryable(EasyQuerySysUser.class).whereById(1).toList();
	}

	@Override
	public void executeJdbcSql() {
		List<EasyQuerySysUser> easyQuerySysUsers = easyQuery.sqlQuery("select * from sys_user where id = ?", EasyQuerySysUser.class, Collections.singletonList(1));
	}

	@Override
	public void executeTemplateSql() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void sqlFile() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void one2Many() {
		EasyQuerySysCustomer sysUser = easyQuery.queryable(EasyQuerySysCustomer.class)
			.include(o -> o.many(EasyQuerySysCustomer::getOrder))
			.where(o->o.eq(EasyQuerySysCustomer::getId,1))
			.firstOrNull(EasyQuerySysCustomer.class);
		int size = sysUser.getOrder().size();
	}

	@Override
	public void pageQuery() {
		EasyPageResult<EasyQuerySysUser> pageResult = easyQuery.queryable(EasyQuerySysUser.class)
			.where(o->o.eq(EasyQuerySysUser::getCode,"用户一"))
			.toPageResult(1, 5);
	}

	@Override
	public void complexMapping() {

		EasyQuerySysCustomer easyQuerySysCustomer = easyQuery.queryable(EasyQuerySysCustomer.class)
			.include(o -> o.many(EasyQuerySysCustomer::getOrder))
			.whereById(1)
			.select(EasyQuerySysCustomer.class, o -> o.columnAll().columnIncludeMany(EasyQuerySysCustomer::getOrder, EasyQuerySysCustomer::getOrder))
			.firstOrNull();
		easyQuerySysCustomer.getOrder().get(0);

	}

	@Override
	public void getAll() {
		List<EasyQuerySysUser> list = easyQuery.queryable(EasyQuerySysUser.class).toList();
	}
}
