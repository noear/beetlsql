package org.beetl.sql.jmh.dbvisitor;

import net.hasor.dbvisitor.dal.mapper.BaseMapper;
import org.beetl.sql.jmh.beetl.vo.BeetlSQLSysUser;

public interface DBVisitorMapper extends BaseMapper<BeetlSQLSysUser> {
}
